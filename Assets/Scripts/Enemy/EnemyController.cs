﻿using System.Collections;
using System.Collections.Generic;
using Spaceship;
using UnityEngine;
using UnityEngine.Serialization;

namespace Enemy
{
    public class EnemyController : MonoBehaviour
    {
        [SerializeField] private EnemySpaceship enemySpaceship;
        

        private Transform taget;

        private void Start()
        {
            taget = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        }
        private void Update()
        {
            MoveToPlayer();
            enemySpaceship.Fire();
        }

        private void MoveToPlayer()
        {
            transform.position = Vector2.MoveTowards(transform.position, taget.transform.position - transform.position, enemySpaceship.Speed * Time.deltaTime);
        }
    }    
}

