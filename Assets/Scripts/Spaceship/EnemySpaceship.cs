using System;
using Manager;
using UnityEngine;

namespace Spaceship
{
    public class EnemySpaceship : Basespaceship, IDamagable
    {
        public event Action OnExploded;


        [SerializeField] private double fireRate = 1;
        

       


        private float fireCounter = 0;

        private void Update()
        {
            //Fire();
        }
        public void Init(int hp, float speed)
        {
            base.Init(hp, speed, defaultBullet);
        }
        public void TakeHit(int damage)
        {
            SoundManager.Instance.Play(SoundManager.Instance.AudioSorceForAction, SoundManager.Sound.EnemyTakehit);
            healthbar.SetHealth(Hp);
            Hp -= damage;

            if (Hp > 0)
            {
                return;
            }
            
            Explode();
        }

        public void Explode()
        {
            SoundManager.Instance.Play(SoundManager.Instance.AudioSorceForAction, SoundManager.Sound.Explod);

            Debug.Assert(Hp <= 0, "HP is more than zero");
            gameObject.SetActive(false);
            Destroy(gameObject);
            OnExploded?.Invoke();
        }

        public override void Fire()
        {
            fireCounter += Time.deltaTime;
            if(fireCounter>=fireRate)
            {
                SoundManager.Instance.Play(SoundManager.Instance.AudioSorceForAction, SoundManager.Sound.Enemyfire);

                var bullet = Instantiate(defaultBullet, gunPosition.position, Quaternion.identity);
                bullet.Init(Vector2.down);
                fireCounter = 0;
            }
            
        }
      

    }
}