using Manager;
using System;
using UnityEngine;

namespace Spaceship
{
    public class PlayerSpaceship : Basespaceship, IDamagable, IGetItem
    {
        public event Action OnExploded;

        [SerializeField] private Bullet supperBullet;
        

        bool useSupperBullet = false;
        public enum BulletType
        {
            NormalBullet,
            SupperBullet,
        }

        private void Awake()
        {
            Debug.Assert(defaultBullet != null, "defaultBullet cannot be null");
            Debug.Assert(gunPosition != null, "gunPosition cannot be null");
           
        }

        public bool GetItem(string bulletType)
        {

            if (bulletType == "SupperBullet")
            {
                useSupperBullet = true;

            }

            return useSupperBullet;
        }



        public void Init(int hp, float speed)
        {
            base.Init(hp, speed, defaultBullet);
        }

        public override void Fire()
        {
            
            if (!useSupperBullet)
            {
                //ADD FireSound
                SoundManager.Instance.Play(SoundManager.Instance.AudioSorceForAction, SoundManager.Sound.Playerfire);

                var bullet = Instantiate(defaultBullet, gunPosition.position, Quaternion.identity);
                bullet.Init(Vector2.up);
            }
            else
            {
                SoundManager.Instance.Play(SoundManager.Instance.AudioSorceForAction, SoundManager.Sound.PlayerfireSupperBullet);
                var bullet = Instantiate(supperBullet, gunPosition.position, Quaternion.identity);
                bullet.Init(Vector2.up);
            }


        }

        public void TakeHit(int damage)
        {
            

            SoundManager.Instance.Play(SoundManager.Instance.AudioSorceForAction, SoundManager.Sound.PlayerTakehit);

            Hp -= damage;
            healthbar.SetHealth(Hp);
            if (Hp > 0)
            {
                return;
            }
            Explode();
        }

        public void Explode()
        {
            SoundManager.Instance.Play(SoundManager.Instance.AudioSorceForAction, SoundManager.Sound.Explod);

            Debug.Assert(Hp <= 0, "HP is more than zero");
            Destroy(gameObject);
            OnExploded?.Invoke();
        }

       
        
    }
}