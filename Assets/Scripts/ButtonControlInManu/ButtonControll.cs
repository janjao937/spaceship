﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Manager;

namespace ButtonControlInManu
{
    public class ButtonControll : MonoBehaviour
    {

        [SerializeField] private Button button;
        // Start is called before the first frame update
        void Start()
        {
            button.onClick.AddListener(OnButtonClicked);
        }

        private void OnButtonClicked()
        {
            SoundManager.Instance.Play(SoundManager.Instance.AudioSorceForAction, SoundManager.Sound.ButtonClick);
            SceneManager.LoadScene("Game");
        }
    }
}
