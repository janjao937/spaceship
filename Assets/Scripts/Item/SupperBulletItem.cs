﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Manager;

namespace Item
{
    public class SupperBulletItem : MonoBehaviour
    {
        [SerializeField] string supperBullet = "SupperBullet";
        [SerializeField] Rigidbody2D rb2D;

        private void Awake()
        {
            rb2D.AddTorque(40f);
        }
        

        private void OnTriggerEnter2D(Collider2D collision)
        {
            SoundManager.Instance.Play(SoundManager.Instance.AudioSorceForAction, SoundManager.Sound.GetItem);
            var item = collision.gameObject.GetComponent<IGetItem>();
            item?.GetItem(supperBullet);
            Destroy(gameObject);
        }




    }


}
