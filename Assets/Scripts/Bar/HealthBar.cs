﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Bar
{

    public class HealthBar : MonoBehaviour
    {
        [SerializeField] private Slider healthbar;

        public void Maxhealth(int health)
        {
            healthbar.maxValue = health;
        }
        public void SetHealth(int health)
        {
            healthbar.value = health;
        }
    }
}
