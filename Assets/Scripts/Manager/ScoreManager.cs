﻿using System;
using TMPro;
using UnityEngine;

namespace Manager
{
    public class ScoreManager : MonoSingleton<ScoreManager>
    {
        public int Score { get; private set; }
        [SerializeField] private TextMeshProUGUI ScoreText;
        [SerializeField] private TextMeshProUGUI highScoreText;


        public event Action ScoreUpdate;

        public void Init()
        {
           
            Reset();
        }

        public void SetScore(int score)
        {
            Score = score;
            ScoreText.text = $"Score :{score}";
            SetHighScoreText(score);
            ScoreUpdate?.Invoke();
        }

         public void Reset()
        {
            Score = 0;
            SetHighScoreText(Score);
            ScoreText.text = $"Score :{Score}";
        }

        private void SetHighScoreText(int score)
        {
           
            highScoreText.text = $"Score :{score}";
        }

    }
}


