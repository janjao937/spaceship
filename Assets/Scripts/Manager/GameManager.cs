﻿using System;
using System.Collections.Generic;
using Spaceship;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Item;

namespace Manager
{
    public class GameManager : MonoBehaviour
    {
        [SerializeField] private GameObject UICanvas;
        [SerializeField] private Button startButton;
        [SerializeField] private TextMeshProUGUI startButtonText;
        [SerializeField] private RectTransform dialog;
        [SerializeField] private PlayerSpaceship playerSpaceship;
        [SerializeField] private EnemySpaceship enemySpaceship;
        //item
        [SerializeField] private SupperBulletItem supperBulletItem;

        [SerializeField] private ScoreManager scoreManager;
        public event Action OnRestarted;
        [SerializeField] private int playerSpaceshipHp;
        [SerializeField] private int playerSpaceshipMoveSpeed;
        [SerializeField] private int enemySpaceshipHp;
        [SerializeField] private int enemySpaceshipMoveSpeed;

        [SerializeField] private EnemySpaceship bossSpaceship;
        [SerializeField] private int bossSpaceshipHp;
        [SerializeField] private int bossSpaceshipMoveSpeed;

        [SerializeField] private TextMeshProUGUI levelGame;
        
        [SerializeField] private Image background;
        [SerializeField] private Sprite[] sprites;
        [SerializeField] private int numberToRandomBG;

        [SerializeField] private TextMeshProUGUI winOrLoseText;

       
        private int level = 1;
        private int score =0;
        
        private void Start()
        {
            Debug.Assert(startButton != null, "startButton cannot be null");
            Debug.Assert(dialog != null, "dialog cannot be null");
            Debug.Assert(playerSpaceship != null, "playerSpaceship cannot be null");
            Debug.Assert(enemySpaceship != null, "enemySpaceship cannot be null");
            Debug.Assert(scoreManager != null, "scoreManager cannot be null");
            Debug.Assert(playerSpaceshipHp > 0, "playerSpaceship hp has to be more than zero");
            Debug.Assert(playerSpaceshipMoveSpeed > 0, "playerSpaceshipMoveSpeed has to be more than zero");
            Debug.Assert(enemySpaceshipHp > 0, "enemySpaceshipHp has to be more than zero");
            Debug.Assert(enemySpaceshipMoveSpeed > 0, "enemySpaceshipMoveSpeed has to be more than zero");

            DontDestroyOnLoad(UICanvas);
            SoundManager.Instance.PlayBGM(SoundManager.Sound.BGM);
            startButton.onClick.AddListener(OnStartButtonClicked);
        }
       

        private void OnStartButtonClicked()
        {
            SoundManager.Instance.Play(SoundManager.Instance.AudioSorceForAction, SoundManager.Sound.ButtonClick);
            dialog.gameObject.SetActive(false);
            score = 0;

            RandomBackground();
            levelGame.text = $"Level :{level}";
            StartGame();
           
        }

        private void StartGame()
        {
            level = 1;
            scoreManager.Init();
            SpawnPlayerSpaceship();
            SpawnEnemySpaceship();
            SpawnSupperBulletItem();
            

        }
        
        private void SpawnPlayerSpaceship()
        {
            var spaceship = Instantiate(playerSpaceship);
            spaceship.Init(playerSpaceshipHp, playerSpaceshipMoveSpeed);
            playerSpaceship.healthbar.Maxhealth(playerSpaceshipHp);
            spaceship.OnExploded += OnPlayerSpaceshipExploded;
            
        }

        private void OnPlayerSpaceshipExploded()
        {
            Restart();
        }

        private void SpawnEnemySpaceship()
        {
            var spaceship = Instantiate(enemySpaceship);
            spaceship.Init(enemySpaceshipHp, enemySpaceshipMoveSpeed);
            spaceship.OnExploded += OnEnemySpaceshipExploded;
           
        }

        private void SpawnBossSpaceship()
        {
            var spaceship = Instantiate(bossSpaceship);
            spaceship.Init(bossSpaceshipHp, bossSpaceshipMoveSpeed);
            spaceship.OnExploded += OnEnemySpaceshipExploded;
            
        }

        private void SpawnSupperBulletItem()
        {
            var _supperBulletItem = Instantiate(supperBulletItem);

        }

        private void RandomBackground()
        {
            numberToRandomBG= UnityEngine.Random.Range(0, 4);
            background.sprite = sprites[numberToRandomBG];

        }



        public void OnEnemySpaceshipExploded()
        {
            scoreManager.SetScore(++score);

            if(scoreManager.Score==1)
            {
                SoundManager.Instance.PlayBGM(SoundManager.Sound.BGM2);
                level = 2;
                levelGame.text = $"Level :{level}";
                RandomBackground();
                DontDestroyOnLoad(gameObject);
                SpawnBossSpaceship();
                SpawnEnemySpaceship();


            }
            else if(scoreManager.Score == 3)
            {
                Win();


            }

            
        }

        public void Restart()
        {

            startButtonText.text = $"Restart";
            winOrLoseText.text = $"You Lose";
            DestroyRemainingShip();
            dialog.gameObject.SetActive(true);
            OnRestarted?.Invoke();
        }

        public void Win()
        {
            winOrLoseText.text = $"You Win";
            startButtonText.text = $"Start";
            DestroyRemainingShip();
            dialog.gameObject.SetActive(true);
            OnRestarted?.Invoke();





        }
        
        private void DestroyRemainingShip()
        {
            
            var remainingEnemies = GameObject.FindGameObjectsWithTag("Enemy");
            foreach(var enemy in remainingEnemies )
            {
                Destroy(enemy);
            }

            var remainingPlayers = GameObject.FindGameObjectsWithTag("Player");
            foreach (var player in remainingPlayers)
            {
                Destroy(player);
            }

        }

       

       


    }
}
